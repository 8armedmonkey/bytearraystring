package com.eightam.bytearraystring;

import java.util.Arrays;

public class ByteArrayString implements Wipeable {

    private byte[] value;

    public ByteArrayString() {
        this(new byte[0]);
    }

    public ByteArrayString(byte[] value) {
        if (value != null) {
            this.value = new byte[value.length];
            System.arraycopy(value, 0, this.value, 0, value.length);
        } else {
            this.value = new byte[0];
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ByteArrayString) {
            return Arrays.equals(value, ((ByteArrayString) obj).getValue());
        }
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(value);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        wipe();
    }

    @Override
    public void wipe() {
        if (value != null) {
            Arrays.fill(value, (byte) 0);
        }
    }

    public byte[] getValue() {
        return value;
    }

    public void setValue(byte[] value) {
        wipe();

        if (value != null) {
            this.value = value;
        } else {
            this.value = new byte[0];
        }
    }

}
