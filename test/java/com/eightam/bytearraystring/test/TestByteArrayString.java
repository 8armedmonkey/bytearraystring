package com.eightam.bytearraystring.test;

import com.eightam.bytearraystring.ByteArrayString;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TestByteArrayString {

    @Test
    public void testEqualityWithNoValue() {
        ByteArrayString b1 = new ByteArrayString();
        ByteArrayString b2 = new ByteArrayString();

        Assert.assertTrue(b1.equals(b2));
    }

    @Test
    public void testEqualityWithValue() {
        ByteArrayString b1 = new ByteArrayString("CONTENT".getBytes());
        ByteArrayString b2 = new ByteArrayString("CONTENT".getBytes());
        ByteArrayString b3 = new ByteArrayString(new byte[]{0x01, 0x02, 0x03, 0x04});
        ByteArrayString b4 = new ByteArrayString(new byte[]{0x01, 0x02, 0x03, 0x04});

        Assert.assertTrue(b1.equals(b2));
        Assert.assertTrue(b3.equals(b4));
    }

    @Test
    public void testInequality() {
        ByteArrayString b1 = new ByteArrayString("CONTENT 1".getBytes());
        ByteArrayString b2 = new ByteArrayString("CONTENT 2".getBytes());
        ByteArrayString b3 = new ByteArrayString(new byte[]{0x01, 0x02, 0x03, 0x04});
        ByteArrayString b4 = new ByteArrayString(new byte[]{0x05, 0x06, 0x07, 0x08});

        Assert.assertFalse(b1.equals(b2));
        Assert.assertFalse(b3.equals(b4));
    }

    @Test
    public void testHashMapKey() {
        ByteArrayString b1 = new ByteArrayString("KEY 1".getBytes());
        ByteArrayString b2 = new ByteArrayString("KEY 2".getBytes());
        ByteArrayString b3 = new ByteArrayString(new byte[]{0x01, 0x02, 0x03, 0x04});

        Map<ByteArrayString, String> map = new HashMap<>();
        map.put(b1, "VALUE 1");
        map.put(b2, "VALUE 2");
        map.put(b3, "VALUE 3");

        Assert.assertEquals("VALUE 1", map.get(new ByteArrayString("KEY 1".getBytes())));
        Assert.assertEquals("VALUE 2", map.get(new ByteArrayString("KEY 2".getBytes())));
        Assert.assertEquals("VALUE 3", map.get(new ByteArrayString(new byte[]{0x01, 0x02, 0x03, 0x04})));
        Assert.assertNull(map.get(new ByteArrayString("KEY 3".getBytes())));
    }

    @Test
    public void testWipeFromByteArray() {
        ByteArrayString b1 = new ByteArrayString(new byte[]{0x01, 0x02, 0x03, 0x04});
        ByteArrayString b2 = new ByteArrayString(new byte[]{0x00, 0x00, 0x00, 0x00});

        b1.wipe();

        Assert.assertTrue(b1.equals(b2));
    }

    @Test
    public void testWipeFromString() {
        ByteArrayString b = new ByteArrayString("KEY".getBytes());
        byte[] empty = new byte[b.getValue().length];

        Arrays.fill(empty, (byte) 0);
        b.wipe();

        Assert.assertTrue(Arrays.equals(empty, b.getValue()));
    }

}
